# GetVMSA-2018-0025VMS

VMSA-2018-0025 – VMware ESXi, Workstation, and Fusion workarounds address a denial-of-service vulnerability

https://blogs.vmware.com/security/2018/10/new-vmsa-2018-0025-and-intel-graphics-driver-unified-shader-compiler-security-updates.html

VMSA-2018-0025 – VMware ESXi, Workstation, and Fusion workarounds address a denial-of-service vulnerability

# Get a list of affected VMs

- download
- connect to vCenter/ESXi host `Connect-VIServer -Server vcenter.corp.com`
- `Unblock-File .\getKB57796VMS.ps1`
- Execute
`.\getvmsa-2018-0025vms.ps1`

# how it works

 `Get-VM |Get-AdvancedSetting -Name mks.enable3d* |where Value -EQ "TRUE" `



This documents an important severity denial-of-service vulnerability that affects VMware ESXi, Workstation and Fusion. This issue arises due to an infinite loop in the 3D-rendering shader. Successfully exploiting this issue may allow an attacker with normal user privileges in the guest to make the VM unresponsive, and in some cases, possibly result other VMs on the host or the host itself becoming unresponsive. The issue can only be exploited if 3D-acceleration feature is enabled

3D-acceleration feature is enabled by default on Workstation and Fusion. On ESXi, this feature is not enabled by default and this is also true for Horizon 6 & 7, Horizon DaaS Platform for Service Providers, and VMC on AWS.

Because many graphics API’s and hardware lack pre-emption support, a specially crafted 3D shader may loop for an infinite amount of time and lock up a VM’s virtual graphics device. Such a shader cannot always be validated by VMware hypervisors, since it may be well-formed but still cause problems if designed to run for an extremely long time. In such cases, VMware hypervisors then rely on the host’s graphics driver to ensure that other users of 3D graphics on the host are not impacted by the malicious VM. However, many graphics drivers may themselves get into to a denial-of-service condition caused by such infinite shaders, and as a result other VMs or processes running on the host might also be affected.

There is no patch for this issue, customers must review their risk and apply the workarounds if applicable. We have released workarounds documented in VMSA-2018-0025.

We would like to thank Piotr Bania of Cisco Talos for reporting this issue to us.

Please sign up to the Security-Announce mailing list to receive new and updated VMware Security Advisories.

Also, we wanted to make you aware that Intel has released a security bulletin entitled “INTEL-SA-00166 – Multiple potential security vulnerabilities in Intel Graphics Drivers may allow escalation of privilege or denial of service . Intel is releasing Intel Graphics Driver updates to mitigate these potential vulnerabilities”.

It documents the remediation of CVE-2018-12152, CVE-2018-12153 and CVE-2018-12154. These issues have been shown to affect VMware Workstation running on Windows. Therefore, we wanted to make sure you are informed of these issues so that they can be appropriately mitigated by the updates that Intel has provided in INTEL-SA-00166.

Customers should review the available documentation and direct technical inquiries to VMware Support for further assistance.


VMSA-2018-0025
VMware ESXi, Workstation, and Fusion workarounds address a denial-of-service vulnerability
VMware Security Advisory
 
Advisory ID:	VMSA-2018-0025
Severity:	Important
Synopsis:	VMware ESXi, Workstation, and Fusion workarounds address a denial-of-service vulnerability
Issue date:	2018-10-09
Updated on:	2018-10-09 (Initial Advisory)
CVE numbers:	CVE-2018-6977
1. Summary
VMware ESXi, Workstation, and Fusion workarounds address a denial-of-service vulnerability
2. Relevant Products
VMware vSphere ESXi (ESXi)
VMware Workstation Pro / Player (Workstation)
VMware Fusion Pro, Fusion (Fusion)
3. Problem Description
Denial-of-service vulnerability in 3D-acceleration feature
 

VMware ESXi, Workstation and Fusion contain a denial-of-service vulnerability due to an infinite loop in a 3D-rendering shader. Successfully exploiting this issue may allow an attacker with normal user privileges in the guest to make the VM unresponsive, and in some cases, possibly result other VMs on the host or the host itself becoming unresponsive.

 
Because many graphics API's and hardware lack pre-emption support, a specially crafted 3D shader may loop for an infinite amount of time and lock up a VM's virtual graphics device. Such a shader cannot always be validated by VMware hypervisors, since it may be well-formed but still cause problems if designed to run for an extremely long time. In such cases, VMware hypervisors then rely on the host's graphics driver to ensure that other users of 3D graphics on the host are not impacted by the malicious VM. However, many graphics drivers may themselves get into to a denial-of-service condition caused by such infinite shaders, and as a result other VMs or processes running on the host might also be affected.
   
The workaround for this issue requires disabling the 3D-acceleration feature as documented in the Mitigation/Workaround column of the below table.
  

The issue can only be exploited if 3D-acceleration feature is enabled. It is not enabled by default on ESXi and is enabled by default on Workstation and Fusion. The 3D-acceleration settings can be reviewed as follows.
  

ESXi
 With Host Client or vCenter, go to the individual VM > configure >  hardware > video card >

 3D Graphics --> Check if "3D Graphics" is enabled.
    OR  

 Go to individual VMX file and then check for "mks.enable3d", if the VMs have the option  

 "mks.enable3d=TRUE", then 3D-acceleration  feature is enabled
  

Workstation
   - Select virtual machine and select VM > Settings.
   - On the Hardware tab, select Display
    If the "Accelerate 3D graphics" is checked then 3D-acceleration feature is enabled.
   
 Fusion
   -From the VMware Fusion menu bar, select Window > Virtual Machine Library.
   -Select a virtual machine and click Settings.
   -In the Settings Window > select Display.
    If the "Accelerate 3D graphics" is checked then 3D-acceleration feature is enabled.
  

VMware would like to thank Piotr Bania of Cisco Talos for reporting this issue to us.
  

The Common Vulnerabilities and Exposures project (cve.mitre.org) has assigned the identifier CVE-2018-6977 to this issue.
 

Column 5 of the following table lists the action required to remediate the vulnerability in each release, if a solution is available.

VMware Product
Product Version
Running on
Severity
Replace with/ Apply Patch*
Mitigation/ Workaround
ESXi	Any	Any	Important	n/a	See references
Workstation	Any	Any	Important	n/a	
Fusion	Any	OS X	Important	n/a	
 

*There is no patch for this issue, customers must review their risk and apply the workarounds if applicable.

 

4. Solution

Please see the above table for Mitigation/Workaround.
 

5. References

https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-6977

https://kb.vmware.com/s/article/59146

 

https://www.vmware.com/in/security/hardening-guides.html

Item 34, vm.disable-non-essential-3D-features of the vSphere Security Configuration Guide for 6.5 Update 1
 

6. Change log
 

VMSA-2018-0025 2018-10-09

Initial security advisory documenting workarounds for VMware ESXi, Workstation and Fusion on 2018-10-09.
 

7. Contact
 

E-mail list for product security notifications and announcements:

http://lists.vmware.com/cgi-bin/mailman/listinfo/security-announce

 

This Security Advisory is posted to the following lists:

  security-announce@lists.vmware.com

  bugtraq@securityfocus.com

  fulldisclosure@seclists.org

 

E-mail: security@vmware.com

PGP key at:

https://kb.vmware.com/kb/1055

 

VMware Security Advisories

http://www.vmware.com/security/advisories

 

VMware Security Response Policy

https://www.vmware.com/support/policies/security_response.html

 

VMware Lifecycle Support Phases

https://www.vmware.com/support/policies/lifecycle.html

 

VMware Security & Compliance Blog  

https://blogs.vmware.com/security

 

Twitter

https://twitter.com/VMwareSRC
 

Copyright 2018 VMware Inc. All rights reserved.