##config

## VMware Security Advisories - VMSA-2018-0025
## https://www.vmware.com/security/advisories/VMSA-2018-0025.html


#check VMware.PowerCLI Modules
if (-not (Get-Module -ListAvailable -Name "VMware.PowerCLI"))
    {Write-Host 'VMWare "VMware.PowerCLI" Module nicht installiert... 
    
    ' -ForegroundColor Red

    Write-Host -NoNewline 'Try: '
    Write-Host -NoNewline -ForegroundColor Green  'Install-Module -Name "VMware.PowerCLI"
    '
    break
    }

#test connection
if (-not ($global:DefaultVIServer))
    {Write-Host 'Not connected to vCenter or ESXi-Host   
    
    ' -ForegroundColor Red
    Write-Host -NoNewline 'Try: '
    Write-Host -NoNewline -ForegroundColor Green  'Connect-VIServer -Server vcenter.corp.com
    '
    break
    }

#start
$affectedVMs = Get-VM |Get-AdvancedSetting -Name mks.enable3d* |where Value -EQ "TRUE"

#Out Affected VMs
$affectedVMs | Sort-Object Entity | Format-Table -AutoSize Entity,Name,Value


if ($affectedVMs -eq $null) {Write-Host -ForegroundColor Cyan '
No VMs are affected ...
'}


